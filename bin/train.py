#!/bin/python 

import os
import sys

project_path, x = os.path.split(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_path)

import tensorflow as tf
from keras.optimizers import Adam
from keras.callbacks import  EarlyStopping

from bnn.model import create_bayesian_model
from bnn.loss_equations import bayesian_categorical_crossentropy_NEW
from bnn.data import test_train_data, get_test_data
import numpy as np
from keras import backend as K


flags = tf.app.flags
FLAGS = flags.FLAGS

flags.DEFINE_string('dataset', 'belochki', 'The dataset to train the model on.')
flags.DEFINE_string('encoder', 'resnet50', 'The encoder model to train from.')
flags.DEFINE_integer('epochs', 1, 'Number of training examples.')
flags.DEFINE_integer('monte_carlo_simulations', 100, 'The number of monte carlo simulations to run for the aleatoric categorical crossentroy loss function.')
flags.DEFINE_integer('batch_size', 64, 'The batch size for the generator')
flags.DEFINE_boolean('debug', False, 'If this is for debugging the model/training process or not.')
flags.DEFINE_integer('verbose', 1, 'Whether to use verbose logging when constructing the data object.')
flags.DEFINE_boolean('stop', True, 'Stop aws instance after finished running.')
flags.DEFINE_float('min_delta', 0.005, 'Early stopping minimum change value.')
flags.DEFINE_integer('patience', 20, 'Early stopping epochs patience to wait before stopping.')


def q8_accuracy(y_true, y_pred):
    return K.sum(K.cast(K.equal(K.argmax(y_true, axis=-1), K.argmax(y_pred, axis=-1)), 'float32') * K.sum(y_true,
                                                                                                          axis=-1)) / K.sum(y_true)

def main(_):
	np.random.seed(2)
	((x_train, x_train_aux,  y_train), (x_test, x_test_aux, y_test)) = test_train_data()
	(x_test, x_test_aux, y_test) = get_test_data()
	num_classes = y_train.shape[-1]

	model = create_bayesian_model(num_classes)
	print(model.summary())
	callbacks = [
		EarlyStopping(monitor='val_logits_variance_loss', min_delta=FLAGS.min_delta, patience=FLAGS.patience, verbose=1)
	]

	print("Compiling model.")
	model.compile(
		optimizer=Adam(lr=1e-3, decay=0.001),
		loss={
		'logits_variance': bayesian_categorical_crossentropy_NEW(FLAGS.monte_carlo_simulations, num_classes),
		'softmax_output': 'categorical_crossentropy'
		},
        metrics={'softmax_output': q8_accuracy},
		loss_weights={'logits_variance': .2, 'softmax_output': 1.})
	print("Starting model train process.")

	model.fit({'main_input': x_train, 'second_input': x_train_aux},
		{'logits_variance':y_train, 'softmax_output':y_train},
		callbacks=callbacks,
		epochs=FLAGS.epochs,
		verbose=FLAGS.verbose,
		batch_size=FLAGS.batch_size,
		validation_data=({'main_input': x_test, 'second_input': x_test_aux}, {'logits_variance': y_test, 'softmax_output': y_test}))
	print("Finished training model.")
	model.save("model.h5")

if __name__ == '__main__':
	tf.app.run()
