from keras.datasets import cifar10
from keras.applications.resnet50 import preprocess_input
import numpy as np

def test_train_data():
    print("Loading train data (Cullpdb_filted)...")
    # X_in = np.load('./drive/collab/testoftest/dataset/cullpdb+profile_6133_filtered.npy.gz')
    X_in = np.load('../dataset/cullpdb+profile_6133_filtered.npy.gz')

    X = np.reshape(X_in, (5534, 700, 57))
    del X_in

    # for joint learning
    labels_1 = X[:, :, 22:30]  # secondary struture label
    ####
    feature_index = np.hstack((np.arange(0, 21), np.arange(35, 56)))  # 42-d features
    X = X[:, :, feature_index]

    # getting meta
    num_seqs, seqlen, feature_dim = np.shape(X)
    num_classes = 8

    seq_names = np.arange(0, num_seqs)
    # np.random.shuffle(seq_names)
    X_train = X[seq_names[0:5022]]
    labels_1_train = labels_1[seq_names[0:5022]]

    print("Train data shape is", X_train.shape, ", two labels shapes are", labels_1_train.shape)

    ####################################################################
    ##### Validation DATA #####
    ###################################################################
    X_valid = X[seq_names[5022:5534]]
    labels_1_valid = labels_1[seq_names[5022:5534]]

    print("Validation data shape is", X_valid.shape, ", two labels shapes are", labels_1_valid.shape)

    X_train_in1_all = X_train[:, :, 0:21]
    X_train_in2_all = X_train[:, :, 21:42]
    X_valid_in1_all = X_valid[:, :, 0:21]
    X_valid_in2_all = X_valid[:, :, 21:42]

    a = [(np.nonzero(np.array(list(map(lambda x: 1 - np.sum(x), X_train_in1_all[i]))))[0][0],i) for i in range(len(X_train_in1_all))]
    a_sorted = sorted(a, key=lambda tup: tup[0], reverse=True)[:10]
    print(a_sorted)
    return ((X_train_in1_all, X_train_in2_all, labels_1_train), (X_valid_in1_all, X_valid_in2_all, labels_1_valid))

def get_test_data():
    ###########################################################################
    ##### CB513 Test DATA #####
    ##########################################################################
    print("Loading Test data (CB513)...")
    # X_in = np.load('./drive/collab/testoftest/dataset/cb513+profile_split1.npy.gz')
    X_in = np.load('../dataset/cb513+profile_split1.npy.gz')

    X1 = np.reshape(X_in, (514, 700, 57))
    X = X1
    del X_in, X1

    # for joint learning
    labels_1 = X[:, :, 22:30]  # secondary struture label

    feature_index = np.hstack((np.arange(0, 21), np.arange(35, 56)))  # 42-d features
    X = X[:, :, feature_index]

    # getting meta
    num_seqs, seqlen, feature_dim = np.shape(X)
    num_classes = 8

    # When test, we use batch_size = 1, so there is no need for padding
    X_test_cb513 = X
    labels_1_test_cb513 = labels_1
    X_test1 = X_test_cb513[:, :, 0:21]
    X_test2 = X_test_cb513[:, :, 21:42]
    return (X_test1, X_test2, labels_1_test_cb513)

test_train_data()