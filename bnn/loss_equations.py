import numpy as np
from keras import backend as K
from tensorflow.contrib import distributions

# Modified loss function for aleatoric uncertainty
# N data points, C classes, T monte carlo simulations
# true - true values. Shape: (N, 700, 8)
# pred_var - predicted logit values and variance. Shape: (N, 700, 8 + 1)
# returns - loss (N,)
def bayesian_categorical_crossentropy_NEW(T, num_classes):
    def bayesian_categorical_crossentropy_internal(true, pred_var):
        # shape: (N, 700)
        std = K.sqrt(pred_var[:, :, num_classes:])
        # shape: (N, 700)
        variance = pred_var[:, :, num_classes]
        variance_depressor = K.exp(variance) - K.ones_like(variance)
        # shape: (N, 700, 8)
        pred = pred_var[:, :, 0:num_classes]
        # shape: (N, 700)
        undistorted_loss = K.categorical_crossentropy(pred, true, from_logits=True)
        # shape: (T,)
        iterable = K.variable(np.ones(T))
        dist = distributions.Normal(loc=K.zeros_like(std), scale=std)
        monte_carlo_results = K.map_fn(
            gaussian_categorical_crossentropy(true, pred, dist, undistorted_loss, num_classes), iterable,
            name='monte_carlo_results')
        variance_loss = K.mean(monte_carlo_results, axis=0) * undistorted_loss
        res = variance_loss + undistorted_loss + variance_depressor
        return res
    return bayesian_categorical_crossentropy_internal


# for a single monte carlo simulation,
#   calculate categorical_crossentropy of
#   predicted logit values plus gaussian
#   noise vs true values.
def gaussian_categorical_crossentropy(true, pred, dist, undistorted_loss, num_classes):
  def map_fn(i):
    std_samples = K.permute_dimensions(dist.sample(num_classes), (3, 1, 2, 0))
    distorted_loss = K.categorical_crossentropy(pred + std_samples, true, from_logits=True)
    diff = undistorted_loss - distorted_loss
    return -K.elu(diff)
  return map_fn
