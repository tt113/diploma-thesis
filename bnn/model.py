#!/bin/python 

from keras.applications.resnet50 import ResNet50
import numpy as np
# from keras.applications.resnet50 import preprocess_input, decode_predictions
from keras.models import Model, load_model, Sequential
from keras.layers import Dense, Input, Flatten, Dropout, Activation, Lambda, RepeatVector, Conv1D
from keras.layers.normalization import BatchNormalization
from keras.layers.merge import concatenate
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.layers import Input, Embedding, LSTM, Dense, Lambda, GRU, TimeDistributed, Reshape, Permute, Conv2D, Masking #TimeDistributedDense
from keras.layers.merge import Concatenate
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.utils.generic_utils import get_custom_objects
from bnn.loss_equations import bayesian_categorical_crossentropy_NEW

def load_full_model(encoder, input_shape):
	l_in_1 = Input(shape=(700, 21), name='main_input')
	l_in_2 = Input(shape=(700, 21), name='second_input')

	l_in_1_embed = Dense(50, activation='relu')(l_in_1)
	l_1 = Concatenate(axis=-1)([l_in_1_embed, l_in_2])

	l_in = Reshape((1, 700, 21 + 50))(l_1)

	l_conv_a = Conv2D(64, (3, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_a1 = Permute((2, 1, 3))(l_conv_a)
	l_conv_a2 = Reshape((700, 64))(l_conv_a1)
	l_conv_a2_ = BatchNormalization()(l_conv_a2)

	l_conv_b = Conv2D(64, (7, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_b1 = Permute((2, 1, 3))(l_conv_b)
	l_conv_b2 = Reshape((700, 64))(l_conv_b1)
	l_conv_b2_ = BatchNormalization()(l_conv_b2)

	l_conv_c = Conv2D(64, (11, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_c1 = Permute((2, 1, 3))(l_conv_c)
	l_conv_c2 = Reshape((700, 64))(l_conv_c1)
	l_conv_c2_ = BatchNormalization()(l_conv_c2)

	l_c_a = Concatenate(axis=-1)([l_conv_a2_, l_conv_b2_, l_conv_c2_])
	l_c_a_bn = BatchNormalization()(l_c_a)

	l_forward1 = GRU(units=300, return_sequences=True, dropout=0.5)(l_c_a_bn)
	l_backward1 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_c_a_bn)
	l_bgru1 = Concatenate(axis=-1)([l_forward1, l_backward1])

	l_forward2 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru1)
	l_backward2 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru1)
	l_bgru2 = Concatenate(axis=-1)([l_forward2, l_backward2])

	l_forward3 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru2)
	l_backward3 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru2)
	l_sum = Concatenate(axis=-1)([l_forward3, l_backward3, l_c_a_bn])

	l_reshape_b = Reshape((-1, 300 + 300 + 64 + 64 + 64))(l_sum)

	l_2 = Dense(300, activation='relu')(l_reshape_b)
	l_2_dropout = Dropout(0.5)(l_2)

	l_3 = Dense(300, activation='relu')(l_2)
	l_3_dropout = Dropout(0.5)(l_3)

	l_recurrent_out1 = Dense(8, activation='softmax')(l_3)

	model = Model(inputs=[l_in_1, l_in_2], output=l_recurrent_out1)
	encoder_model = model
	for layer in encoder_model.layers:
		layer.trainable = False
	x = BatchNormalization(name='post_encoder')(l_3_dropout)
	x = Dropout(0.5)(x)
	x = Dense(500, activation='relu')(x)
	x = BatchNormalization()(x)
	x = Dropout(0.5)(x)
	x = Dense(100, activation='relu')(x)
	x = BatchNormalization()(x)
	x = Dropout(0.5)(x)
	logits = Dense(8)(x)
	variance_pre = Dense(1)(x)
	variance = Activation('softplus', name='variance')(variance_pre)
	logits_variance = concatenate([logits, variance], name='logits_variance')
	softmax_output = Activation('softmax', name='softmax_output')(logits)
	model = Model(inputs=encoder_model.input, outputs=[logits_variance, softmax_output])
	best_model_file = '../bnnka.h5'
	print(best_model_file)
	model.load_weights(best_model_file)
	return model

def load_bayesian_model(monte_carlo_simulations=100, classes=8):
	get_custom_objects().update({"bayesian_categorical_crossentropy_internal": bayesian_categorical_crossentropy_NEW(monte_carlo_simulations, classes)})
	return load_model('../bnn/deepModel+BNN.h5')

def create_bayesian_model(output_classes):
	l_in_1 = Input(shape=(700, 21), name='main_input')
	l_in_2 = Input(shape=(700, 21), name='second_input')

	l_in_1_embed = Dense(50, activation='relu')(l_in_1)
	l_1 = Concatenate(axis=-1)([l_in_1_embed, l_in_2])

	l_in = Reshape((1, 700, 21+50))(l_1)

	l_conv_a = Conv2D(64, (3, 21+50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_a1 = Permute((2, 1, 3))(l_conv_a)
	l_conv_a2 = Reshape((700, 64))(l_conv_a1)
	l_conv_a2_ = BatchNormalization()(l_conv_a2)

	l_conv_b = Conv2D(64,  (7, 21+50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_b1 = Permute((2, 1, 3))(l_conv_b)
	l_conv_b2 = Reshape((700, 64))(l_conv_b1)
	l_conv_b2_ = BatchNormalization()(l_conv_b2)

	l_conv_c = Conv2D(64, (11, 21+50), padding="same",  activation='relu', strides=1)(l_in)
	l_conv_c1 = Permute((2, 1, 3))(l_conv_c)
	l_conv_c2 = Reshape((700, 64))(l_conv_c1)
	l_conv_c2_ = BatchNormalization()(l_conv_c2)

	l_c_a = Concatenate(axis=-1)([l_conv_a2_, l_conv_b2_, l_conv_c2_])
	l_c_a_bn = BatchNormalization()(l_c_a)

	l_forward1 = GRU(units=300, return_sequences=True, dropout=0.5)(l_c_a_bn)
	l_backward1 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_c_a_bn)
	l_bgru1 = Concatenate(axis=-1)([l_forward1, l_backward1])

	l_forward2 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru1)
	l_backward2 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru1)
	l_bgru2 = Concatenate(axis=-1)([l_forward2, l_backward2])

	l_forward3 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru2)
	l_backward3 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru2)
	l_sum = Concatenate(axis=-1)([l_forward3, l_backward3, l_c_a_bn])

	l_reshape_b = Reshape((-1, 300+300+64+64+64))(l_sum)

	l_2 = Dense(300, activation='relu')(l_reshape_b)
	l_2_dropout = Dropout(0.5)(l_2)

	l_3 = Dense(300, activation='relu')(l_2_dropout)
	l_3_dropout = Dropout(0.5)(l_3)

	# l_recurrent_out1 = Dense(8, activation='softmax')(l_3_dropout)

	# l_3_dropout_flatten = Flatten()(l_3_dropout.output)

	model = Model(inputs=[l_in_1, l_in_2], output=l_3_dropout)
	model.summary()
	# best_model_file = 'modelka.h5'
	# model.load_weights(best_model_file)
	encoder_model = model
	x = BatchNormalization(name='post_encoder')(model.output)
	x = Dropout(0.5)(x)
	x = Dense(500, activation='relu')(x)
	x = BatchNormalization()(x)
	x = Dropout(0.5)(x)
	x = Dense(100, activation='relu')(x)
	x = BatchNormalization()(x)
	x = Dropout(0.5)(x)
	logits = Dense(output_classes)(x)
	variance_pre = Dense(1)(x)
	variance = Activation('softplus', name='variance')(variance_pre)
	logits_variance = concatenate([logits, variance], name='logits_variance')
	softmax_output = Activation('softmax', name='softmax_output')(logits)
	model = Model(inputs=encoder_model.input, outputs=[logits_variance,softmax_output])
	print(model.summary())
	return model


def create_encoder_model():
	l_in_1 = Input(shape=(700, 21), name='main_input')
	l_in_2 = Input(shape=(700, 21), name='second_input')

	l_in_1_embed = Dense(50, activation='relu')(l_in_1)
	l_1 = Concatenate(axis=-1)([l_in_1_embed, l_in_2])

	l_in = Reshape((1, 700, 21 + 50))(l_1)

	l_conv_a = Conv2D(64, (3, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_a1 = Permute((2, 1, 3))(l_conv_a)
	l_conv_a2 = Reshape((700, 64))(l_conv_a1)
	l_conv_a2_ = BatchNormalization()(l_conv_a2)

	l_conv_b = Conv2D(64, (7, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_b1 = Permute((2, 1, 3))(l_conv_b)
	l_conv_b2 = Reshape((700, 64))(l_conv_b1)
	l_conv_b2_ = BatchNormalization()(l_conv_b2)

	l_conv_c = Conv2D(64, (11, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
	l_conv_c1 = Permute((2, 1, 3))(l_conv_c)
	l_conv_c2 = Reshape((700, 64))(l_conv_c1)
	l_conv_c2_ = BatchNormalization()(l_conv_c2)

	l_c_a = Concatenate(axis=-1)([l_conv_a2_, l_conv_b2_, l_conv_c2_])
	l_c_a_bn = BatchNormalization()(l_c_a)

	l_forward1 = GRU(units=300, return_sequences=True, dropout=0.5)(l_c_a_bn)
	l_backward1 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_c_a_bn)
	l_bgru1 = Concatenate(axis=-1)([l_forward1, l_backward1])

	l_forward2 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru1)
	l_backward2 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru1)
	l_bgru2 = Concatenate(axis=-1)([l_forward2, l_backward2])

	l_forward3 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru2)
	l_backward3 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru2)
	l_sum = Concatenate(axis=-1)([l_forward3, l_backward3, l_c_a_bn])

	l_reshape_b = Reshape((-1, 300 + 300 + 64 + 64 + 64))(l_sum)

	l_2 = Dense(300, activation='relu')(l_reshape_b)
	l_2_dropout = Dropout(0.5)(l_2)

	l_3 = Dense(300, activation='relu')(l_2_dropout)
	l_3_dropout = Dropout(0.5)(l_3)

	# l_recurrent_out1 = Dense(8, activation='softmax')(l_3_dropout)

	model = Model(inputs=[l_in_1, l_in_2], output=l_3_dropout)
	model.summary()
	# best_model_file = 'modelka.h5'

	# print(best_model_file)
	# model.load_weights(best_model_file)
	base_model = model

	# # freeze encoder layers to prevent over fitting
	# for layer in base_model.layers:
	# 	layer.trainable = False
	
	# output_tensor = Flatten()(base_model.output)

	model = Model(inputs=base_model.input, outputs=base_model.output)
	return model
