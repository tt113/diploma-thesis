#!/bin/python

from keras.utils.generic_utils import get_custom_objects
from keras import backend as K
import numpy as np
import math
import json
import simplejson

from bnn.data import get_test_data
from bnn.model import load_bayesian_model, load_full_model
from bnn.data import test_train_data
from bnn.loss_equations import bayesian_categorical_crossentropy_NEW

def load_testable_model(encoder, monte_carlo_simulations, num_classes, min_image_size, full_model):
	if full_model:
		model = load_full_model(encoder, min_image_size)
		print("Compiling full testable model.")
		print(model.summary())
		model.compile(
			optimizer='adam',
			loss={'logits_variance': bayesian_categorical_crossentropy_NEW(monte_carlo_simulations, num_classes)},
			metrics={'softmax_output': ['categorical_accuracy', 'top_k_categorical_accuracy']})
	else:
		model = load_bayesian_model()

	return model


class NumpyEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, np.ndarray):
			return obj.tolist()
		return json.JSONEncoder.default(self, obj)

def predict_softmax_aleatoric_uncertainties(batch_size, verbose, debug, full_model,
											x_train, x_train_aux, y_train, x_test, x_test_aux, y_test,
	encoder, dataset, model_batch_size, model_epochs, model_monte_carlo_simulations):
	num_classes = y_train.shape[-1]
	min_image_size = []
	model = load_testable_model(encoder, model_monte_carlo_simulations, num_classes, min_image_size, full_model)

	# test_belok = np.zeros((1, 700, 21))[0]
	# for i in range(700):
	# 	test_belok[i][3]=1
	# test_belok = np.reshape(test_belok, (1, 700, 21))

	# (X_test1, X_test2, labels_1_test_cb513) = get_test_data()


	print("Predicting softmax and aleatoric_uncertainties.")
	predictions_train = model.predict({'main_input': x_train, 'second_input': x_train_aux}, batch_size=batch_size, verbose=verbose)
	# predictions_test = model.predict({'main_input': X_test1, 'second_input': X_test2}, batch_size=batch_size, verbose=verbose)
	# predictions_test = model.predict({'main_input': test_belok, 'second_input': test_belok}, batch_size=batch_size, verbose=verbose)


	def Q8_accuracy(real, pred):
		total = real.shape[0] * real.shape[1]
		correct = 0
		for i in range(real.shape[0]):  # per element in the batch
			for j in range(real.shape[1]):  # per aminoacid residue
				if np.sum(real[i, j, :]) == 0:  # real[i, j, dataset.num_classes - 1] > 0 # if it is padding
					total = total - 1
				else:
					if real[i, j, np.argmax(pred[i, j, :])] > 0:
						correct = correct + 1
		return correct / total


	print(Q8_accuracy(y_train, predictions_train[1]))

	# Shape (N)
	# aleatoric_uncertainties_train = predictions_train[0][:,:,num_classes:]
	# # aleatoric_uncertainties_test = predictions_test[0][:, :, num_classes:]
	#
	# logits_train = predictions_train[0][:,:,0:num_classes]
	# # logits_test = predictions_test[0][:,:,0:num_classes]
	#
	# # Shape (N, C)
	# softmax_train = predictions_train[1]
	# # softmax_test = predictions_test[1]
	#
	# p_train = np.argmax(predictions_train[1], axis=2)
	# # p_test = np.argmax(predictions_test[1], axis=2)
	# l_train = np.argmax(y_train, axis=2)
	# # l_test = np.argmax(labels_1_test_cb513, axis=2)
	# # Shape (N)
	# prediction_comparision_train = np.equal(p_train, l_train).astype(int)
	# # prediction_comparision_test = np.equal(p_train, l_train).astype(int)
	#
	# def get_last_index(x):
	# 	a = np.nonzero(np.array(list(map(lambda x: 1 - np.sum(x), x))))[0]
	# 	if len(a) == 0:
	# 		return 700
	# 	return a[0]
	#
	# def get_save_object(i, is_train):
	# 	#belok = y_train[i] if is_train else y_test[i]
	# 	belok =  y_train[i]
	# 	belok_p = p_train[i]
	# 	aleatoric_uncertainties = aleatoric_uncertainties_train[i]
	# 	prediction_comparsion =  prediction_comparision_train[i]
	#
	# 	last_amino_index = get_last_index(belok)
	# 	return {
	# 	'predicted_label': belok_p[:last_amino_index],
	# 	'label': np.argmax(belok, axis=1)[:last_amino_index],
	# 	'aleatoric_uncertainty':aleatoric_uncertainties[:last_amino_index],
	# 	'is_correct':prediction_comparsion[:last_amino_index]
	# 	}
	#
	#
	# train_results = [get_save_object(i, True) for i in range(len(prediction_comparision_train))]
	#
	# # test_results = [get_save_object(i, False) for i in range(len(prediction_comparision_test))]
	#
	# # print(train_results)
	# # print(test_results)
	#
	# json_dump = json.dumps(train_results, cls=NumpyEncoder)
	#
	# with open('dddfgdfg.txt', 'w') as f:
	# 	json.dump(json_dump, f)
	# 	f.write("==========================")
	# 	# json_dump = json.dumps(test_results, cls=NumpyEncoder)
	# 	# simplejson.dump(json_dump, f)
	#
	#
	# dictr = {}
	# for res in train_results:
	# 	for i in range(len(res['label'])):
	# 		if res['label'][i] not in dictr:
	# 			dictr[res['label'][i]] = []
	# 		dictr[res['label'][i]].append( (res['aleatoric_uncertainty'][i][0], res['is_correct'][i]))
	# print(dictr)
	# print([(i, np.mean(np.array([x[0] for x in dictr[i]])),
	#   str(np.sum(np.array([x[1] for x in dictr[i]]))) + '/' + str(len([x[1] for x in dictr[i]]))) for i in range(8) if i in dictr])
	#

	return (None, None)

def predict_on_data(batch_size, verbose, epistemic_monte_carlo_simulations, debug, full_model,
					x_train, x_train_aux, y_train, x_test, x_test_aux, y_test,
	encoder, dataset, model_batch_size, model_epochs, model_monte_carlo_simulations, include_epistemic_uncertainty=False):

	(train_results, test_results) = predict_softmax_aleatoric_uncertainties(batch_size, verbose, debug, full_model,
										x_train, x_train_aux, y_train, x_test, x_test_aux, y_test,
		encoder, dataset, model_batch_size, model_epochs, model_monte_carlo_simulations)

	return (train_results, test_results)


def predict(batch_size, verbose, epistemic_monte_carlo_simulations, debug, full_model,
	encoder, dataset, model_batch_size, model_epochs, model_monte_carlo_simulations):
	if full_model:
		((x_train, x_train_aux, y_train), (x_test, x_test_aux, y_test)) = test_train_data()
	else:
		((x_train, x_train_aux, y_train), (x_test, x_test_aux, y_test)) = test_train_data()

	return predict_on_data(batch_size, verbose, epistemic_monte_carlo_simulations, debug, full_model,
						   x_train, x_train_aux, y_train, x_test, x_test_aux, y_test,
		encoder, dataset, model_batch_size, model_epochs, model_monte_carlo_simulations)


