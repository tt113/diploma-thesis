import numpy as np
from keras.layers import Input, BatchNormalization, Activation, Dense, Lambda, Conv2D, GRU, Reshape, Permute, Conv1D, Dropout, Masking #TimeDistributedDense
from keras.layers.merge import Concatenate
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import Model
from keras.layers.merge import concatenate



from keras.utils import plot_model

np.random.seed(2345)

# print("Loading train data (Cullpdb_filted)...")
# X_in = np.load('./drive/collab/testoftest/dataset/cullpdb+profile_6133_filtered.npy.gz')
# # X_in = np.load('dataset/cullpdb+profile_6133_filtered.npy.gz')
#
# X = np.reshape(X_in, (5534,700,57))
# del X_in
#
# # for joint learning
# labels_1 = X[:, :, 22:30]    # secondary struture label
# ####
# feature_index = np.hstack((np.arange(0,21),np.arange(35,56)))  # 42-d features
# X = X[:, :, feature_index]
#
# # getting meta
# num_seqs, seqlen, feature_dim = np.shape(X)
# num_classes = 8
#
# seq_names = np.arange(0, num_seqs)
# np.random.shuffle(seq_names)
# X_train = X[seq_names[0:5022]]
# labels_1_train = labels_1[seq_names[0:5022]]
#
# print ("Train data shape is", X_train.shape, ", two labels shapes are", labels_1_train.shape)
#
# ###################################################################
# #### Validation DATA #####
# ##################################################################
# X_valid = X[seq_names[5022:5534]]
# labels_1_valid = labels_1[seq_names[5022:5534]]
#
# print ("Validation data shape is", X_valid.shape, ", two labels shapes are", labels_1_valid.shape)
#
#
# X_train_in1_all = X_train[:, :, 0:21]
# X_train_in2_all = X_train[:, :, 21:42]
# X_valid_in1_all = X_valid[:, :, 0:21]
# X_valid_in2_all = X_valid[:, :, 21:42]

##########################################################################
#### CB513 Test DATA #####
#########################################################################
print("Loading Test data (CB513)...")
# X_in = np.load('./drive/collab/testoftest/dataset/cb513+profile_split1.npy.gz')
X_in = np.load('dataset/cb513+profile_split1.npy.gz')

X1 = np.reshape(X_in,(514,700,57))
X = X1
del X_in, X1

# for joint learning
labels_1 = X[:, :, 22:30] # secondary struture label

feature_index = np.hstack((np.arange(0,21),np.arange(35,56)))  # 42-d features
X = X[ :, :, feature_index]

# getting meta
num_seqs, seqlen, feature_dim = np.shape(X)
num_classes = 8

# When test, we use batch_size = 1, so there is no need for padding
X_test_cb513 = X
labels_1_test_cb513 = labels_1

print ("Test data shape is", X_test_cb513.shape, ", two labels shapes are", labels_1_test_cb513.shape)

l_in_1 = Input(shape=(700, 21), name='main_input')
l_in_2 = Input(shape=(700, 21), name='second_input')

l_in_1_embed = Dense(50, activation='relu')(l_in_1)
l_1 = Concatenate(axis=-1)([l_in_1_embed, l_in_2])

l_in = Reshape((1, 700, 21 + 50))(l_1)

l_conv_a = Conv2D(64, (3, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
l_conv_a1 = Permute((2, 1, 3))(l_conv_a)
l_conv_a2 = Reshape((700, 64))(l_conv_a1)
l_conv_a2_ = BatchNormalization()(l_conv_a2)

l_conv_b = Conv2D(64, (7, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
l_conv_b1 = Permute((2, 1, 3))(l_conv_b)
l_conv_b2 = Reshape((700, 64))(l_conv_b1)
l_conv_b2_ = BatchNormalization()(l_conv_b2)

l_conv_c = Conv2D(64, (11, 21 + 50), padding="same", activation='relu', strides=1)(l_in)
l_conv_c1 = Permute((2, 1, 3))(l_conv_c)
l_conv_c2 = Reshape((700, 64))(l_conv_c1)
l_conv_c2_ = BatchNormalization()(l_conv_c2)

l_c_a = Concatenate(axis=-1)([l_conv_a2_, l_conv_b2_, l_conv_c2_])
l_c_a_bn = BatchNormalization()(l_c_a)

l_forward1 = GRU(units=300, return_sequences=True, dropout=0.5)(l_c_a_bn)
l_backward1 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_c_a_bn)
l_bgru1 = Concatenate(axis=-1)([l_forward1, l_backward1])

l_forward2 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru1)
l_backward2 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru1)
l_bgru2 = Concatenate(axis=-1)([l_forward2, l_backward2])

l_forward3 = GRU(units=300, return_sequences=True, dropout=0.5)(l_bgru2)
l_backward3 = GRU(units=300, return_sequences=True, go_backwards=True, dropout=0.5)(l_bgru2)
l_sum = Concatenate(axis=-1)([l_forward3, l_backward3, l_c_a_bn])

l_reshape_b = Reshape((-1, 300 + 300 + 64 + 64 + 64))(l_sum)

l_2 = Dense(300, activation='relu')(l_reshape_b)
l_2_dropout = Dropout(0.5)(l_2)

l_3 = Dense(300, activation='relu')(l_2)
l_3_dropout = Dropout(0.5)(l_3)

l_recurrent_out1 = Dense(8, activation='softmax')(l_3)

model = Model(inputs=[l_in_1, l_in_2], output=l_recurrent_out1)
model.summary()
# best_model_file = './drive/collab/modelka.h5'
# model.load_weights(best_model_file)
encoder_model = model
for layer in encoder_model.layers:
    layer.trainable = False
x = BatchNormalization(name='post_encoder')(l_3_dropout)
x = Dropout(0.5)(x)
x = Dense(500, activation='relu')(x)
x = BatchNormalization()(x)
x = Dropout(0.5)(x)
x = Dense(100, activation='relu')(x)
x = BatchNormalization()(x)
x = Dropout(0.5)(x)
logits = Dense(8)(x)
variance_pre = Dense(1)(x)
variance = Activation('softplus', name='variance')(variance_pre)
logits_variance = concatenate([logits, variance], name='logits_variance')
softmax_output = Activation('softmax', name='softmax_output')(logits)
model = Model(inputs=encoder_model.input, outputs=[logits_variance, softmax_output])


#
# best_model_file = './modelka.h5'
# print(best_model_file)
# best_model = ModelCheckpoint(best_model_file, verbose=1, save_best_only = True)
#
# # model.fit({'main_input': X_train_in1_all, 'second_input': X_train_in2_all},
# #            labels_1_train,
# #           epochs=150,
# #           batch_size=128,
# #           callbacks=[best_model],
# #           validation_data=(
# #               {'main_input': X_valid_in1_all, 'second_input': X_valid_in2_all},
# #               labels_1_valid),
# #           verbose=1)
#
#
def Q8_accuracy(real, pred):
    total = real.shape[0] * real.shape[1]
    correct = 0
    for i in range(real.shape[0]):  # per element in the batch
        for j in range(real.shape[1]): # per aminoacid residue
            if np.sum(real[i, j, :]) == 0:  #  real[i, j, dataset.num_classes - 1] > 0 # if it is padding
                total = total - 1
            else:
                if real[i, j, np.argmax(pred[i, j, :])] > 0:
                    correct = correct + 1
    return correct / total


best_model_file = 'bnnka.h5'
print(best_model_file)
model.load_weights(best_model_file)
X_test1 = X_test_cb513[:, :, 0:21]
X_test2 = X_test_cb513[:, :, 21:42]
predictions = model.predict({'main_input': X_test1, 'second_input': X_test2},
           batch_size = 64, verbose=1)
print(predictions)

print(Q8_accuracy(labels_1_test_cb513, predictions[1]))

